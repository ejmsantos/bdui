package com.eduardosantos.bduipoc.extensions

import android.graphics.drawable.Drawable
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat

internal fun View.getCompatDrawable(@DrawableRes drawableRes: Int): Drawable {
    return ContextCompat.getDrawable(context, drawableRes)!!
}

internal fun View.getCompatColor(@ColorRes colorRes: Int): Int {
    return ContextCompat.getColor(context, colorRes)
}

internal fun View.visible() {
    visibility = VISIBLE
}

internal fun View.gone() {
    visibility = GONE
}