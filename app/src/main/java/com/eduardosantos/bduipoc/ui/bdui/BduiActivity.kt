package com.eduardosantos.bduipoc.ui.bdui

import android.os.Bundle
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.eduardosantos.bduipoc.R
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.bdui_activity.*

class BduiActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bdui_activity)
        setSupportActionBar(custom_toolbar)
        supportActionBar?.setHomeButtonEnabled(true);
        setupNavigation()
    }

    private fun setupNavigation() {
        val navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp() =
        Navigation.findNavController(this, R.id.nav_host_fragment).navigateUp()
}