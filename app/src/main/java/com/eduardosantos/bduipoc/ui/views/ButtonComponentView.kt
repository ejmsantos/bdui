package com.eduardosantos.bduipoc.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import com.eduardosantos.bduipoc.R
import com.eduardosantos.bduipoc.extensions.getCompatColor
import com.eduardosantos.bduipoc.extensions.getCompatDrawable
import com.eduardosantos.domain.model.Component
import com.google.android.material.button.MaterialButton

class ButtonComponentView : MaterialButton, BduiComponentBinder {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        layoutParams = ConstraintLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun bindComponentData(component: Component) {
        text = (component as Component.ButtonComponent).text
        background = getCompatDrawable(R.drawable.button_background)
        setTextColor(getCompatColor(R.color.white))
    }


}