package com.eduardosantos.bduipoc.ui.base

import androidx.lifecycle.ViewModel
import com.eduardosantos.bduipoc.di.ViewModelProviderFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseFragment<T : ViewModel> : DaggerFragment(), BaseView<T> {
    @Inject
    protected lateinit var viewModelProviderFactory: ViewModelProviderFactory<T>

}


