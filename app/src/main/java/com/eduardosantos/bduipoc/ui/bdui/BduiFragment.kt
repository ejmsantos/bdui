package com.eduardosantos.bduipoc.ui.bdui

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.eduardosantos.bduipoc.R
import com.eduardosantos.bduipoc.extensions.gone
import com.eduardosantos.bduipoc.extensions.visible
import com.eduardosantos.bduipoc.ui.base.BaseFragment
import com.eduardosantos.bduipoc.ui.views.VerticalSpaceItemDecoration
import com.eduardosantos.bduipoc.ui.model.ViewState
import com.eduardosantos.domain.model.Page
import kotlinx.android.synthetic.main.bdui_fragment.*
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class BduiFragment : BaseFragment<BduiViewModel>() {
    override val viewModel: BduiViewModel by lazy {
        ViewModelProvider(this, viewModelProviderFactory).get(BduiViewModel::class.java)
    }

    private val args: BduiFragmentArgs by navArgs()

    lateinit var recyclerView: RecyclerView

    private val adapter by lazy {
        BduiComponentsAdapter(::onComponentClick)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.bdui_fragment, container, false)
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView(view)

        setupInputs()
        setupOutputs()

        viewModel.onLoadPage(args.path)
    }

    private fun setupRecyclerView(view: View) {
        recyclerView = view.findViewById(R.id.components_view)
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(VerticalSpaceItemDecoration(resources.getDimensionPixelSize(R.dimen.default_spacing)))
    }

    private fun onComponentClick(deepLink: String) {
        viewModel.onNavigateTo(deepLink)
    }

    private fun setupInputs() {
        retry_button.setOnClickListener {
            viewModel.onLoadPage(args.path)
        }
    }

    @InternalCoroutinesApi
    private fun setupOutputs() {
        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.outputs.viewState().collect {
                when (it) {
                    is ViewState.Data -> {
                        if (it.data is Page) {
                            components_view.visible()
                            loading_bar.gone()
                            retry_button.gone()
                            adapter.uiComponents = it.data.items
                            (activity as AppCompatActivity).supportActionBar?.title = it.data.title
                        }
                    }
                    is ViewState.Error -> {
                        components_view.gone()
                        loading_bar.gone()
                        retry_button.visible()
                        Toast.makeText(
                            activity, it.exception.message,
                            Toast.LENGTH_SHORT
                        ).show()
                        (activity as AppCompatActivity).supportActionBar?.title =
                            getString(R.string.error_page_title)
                    }
                    ViewState.Loading -> {
                        components_view.gone()
                        loading_bar.visible()
                        retry_button.gone()
                    }
                }
            }
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewModel.outputs.navigateTo().collect {
                val path = Uri.parse(it).getQueryParameter("path")
                path?.let { validPath ->
                    val action = BduiFragmentDirections.actionPageToPage().setPath(validPath)
                    findNavController().navigate(action)

                    //todo @eduardo.santos try to fix backstack and use navigation deeplink
                    //val intent = Intent(Intent.ACTION_VIEW, Uri.parse("bdui://page?path=components"))
                    //nav_host_fragment.findNavController().handleDeepLink(intent)            }
                }
            }
        }
    }
}