package com.eduardosantos.bduipoc.ui.bdui

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.eduardosantos.bduipoc.ui.views.BduiComponentBinder
import com.eduardosantos.bduipoc.ui.views.ButtonComponentView
import com.eduardosantos.bduipoc.ui.views.CardComponentView
import com.eduardosantos.bduipoc.ui.views.TextComponentView
import com.eduardosantos.domain.model.Component
import java.lang.RuntimeException


class BduiComponentsAdapter(private val actionListener: (path: String) -> Unit) :
    RecyclerView.Adapter<BduiComponentsAdapter.BduiComponentViewHolder>() {

    var uiComponents = emptyList<Component>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemViewType(position: Int): Int {
        return when (uiComponents[position]) {
            is Component.CardComponent -> CARD_COMPONENT_TYPE
            is Component.TextComponent -> TEXT_COMPONENT_TYPE
            is Component.ButtonComponent -> BUTTON_COMPONENT_TYPE
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BduiComponentsAdapter.BduiComponentViewHolder {
        val context: Context = parent.context
        val itemView: View = when (viewType) {
            BUTTON_COMPONENT_TYPE -> ButtonComponentView(context)
            TEXT_COMPONENT_TYPE -> TextComponentView(context)
            CARD_COMPONENT_TYPE -> CardComponentView(parent.context)
            else -> throw RuntimeException("Unsuported component type")
        }

        return BduiComponentViewHolder(itemView, actionListener)
    }

    override fun onBindViewHolder(holder: BduiComponentViewHolder, position: Int) {
        holder.bind(uiComponents[position])
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return uiComponents.size
    }

    class BduiComponentViewHolder(
        itemView: View,
        private val actionListener: (path: String) -> Unit
    ) :
        RecyclerView.ViewHolder(itemView) {
        fun bind(component: Component) {
            when (component) {
                is Component.CardComponent -> {
                    component.action?.let { action ->
                        itemView.setOnClickListener { actionListener.invoke(action) }
                    }
                }
                is Component.ButtonComponent -> {
                    component.action?.let { action ->
                        itemView.setOnClickListener { actionListener.invoke(action) }
                    }
                }
                is Component.TextComponent -> {
                    //no-op
                }
            }
            (itemView as BduiComponentBinder).bindComponentData(component)
        }
    }

    companion object {
        const val CARD_COMPONENT_TYPE = 0
        const val TEXT_COMPONENT_TYPE = 1
        const val BUTTON_COMPONENT_TYPE = 2
    }


}