package com.eduardosantos.bduipoc.ui.views

import com.eduardosantos.domain.model.Component

interface BduiComponentBinder {
    fun bindComponentData(component: Component)
}