package com.eduardosantos.bduipoc.ui.bdui

import android.content.res.Resources
import com.eduardosantos.bduipoc.ui.base.BaseViewModel
import com.eduardosantos.bduipoc.ui.base.BaseViewModelInputs
import com.eduardosantos.bduipoc.ui.base.BaseViewModelOutputs
import com.eduardosantos.bduipoc.ui.model.ViewState
import com.eduardosantos.domain.usecase.BduiUseCaseType
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

interface BduiViewModelInputs : BaseViewModelInputs {
    fun onLoadPage(deepLink: String)
    fun onNavigateTo(deepLink: String)
}

interface BduiViewBuModelOutputs : BaseViewModelOutputs {
    fun navigateTo(): Flow<String>
}

class BduiViewModel @Inject constructor() : BaseViewModel(),
    BduiViewModelInputs, BduiViewBuModelOutputs {

    private val _navigate: MutableSharedFlow<String> = MutableSharedFlow(replay = 0)
    private var navigate: SharedFlow<String> = _navigate

    @Inject
    lateinit var bduiUseCase: BduiUseCaseType

    @Inject
    lateinit var resources: Resources

    override val inputs: BduiViewModelInputs
        get() = this

    override val outputs: BduiViewBuModelOutputs
        get() = this


    override fun onLoadPage(pagePath: String) {
        customScope.launch {
            _viewState.emit(ViewState.Loading)
            try {
                _viewState.emit(ViewState.Data(bduiUseCase.page(pagePath)))
            } catch (exception: Exception) {
                _viewState.emit(ViewState.Error(exception))
            }
        }
    }

    override fun onNavigateTo(deepLink: String) {
        customScope.launch {
            _navigate.emit(deepLink)
        }
    }

    override fun navigateTo(): Flow<String> {
        return navigate
    }
}