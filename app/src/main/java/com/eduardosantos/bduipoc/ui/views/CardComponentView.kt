package com.eduardosantos.bduipoc.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import androidx.constraintlayout.widget.ConstraintLayout
import com.eduardosantos.bduipoc.R
import com.eduardosantos.domain.model.Component
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_component_card.view.*

class CardComponentView : ConstraintLayout, BduiComponentBinder {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    init {
        inflate(context, R.layout.view_component_card, this)
        layoutParams = LayoutParams(MATCH_PARENT, WRAP_CONTENT)
    }

    override fun bindComponentData(component: Component) {
        val card = component as Component.CardComponent

        Picasso.get().load(component.imageUrl).into(card_image)
        card_text.text = card.text
    }


}