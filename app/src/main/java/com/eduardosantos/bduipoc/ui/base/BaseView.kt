package com.eduardosantos.bduipoc.ui.base

import androidx.lifecycle.ViewModel

interface BaseView<T : ViewModel> {
    val viewModel: T
}
