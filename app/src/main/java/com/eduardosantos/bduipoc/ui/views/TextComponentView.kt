package com.eduardosantos.bduipoc.ui.views

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.eduardosantos.bduipoc.extensions.getCompatColor
import com.eduardosantos.domain.model.Component

class TextComponentView : AppCompatTextView, BduiComponentBinder {

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(
        context,
        attrs,
        defStyle
    )

    override fun bindComponentData(component: Component) {
        text = (component as Component.TextComponent).text
        setTextColor(getCompatColor(android.R.color.primary_text_light))
    }
}