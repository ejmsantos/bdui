package com.eduardosantos.bduipoc.ui.model

import java.lang.Exception

sealed class ViewState {
    data class Data(val data: Any) : ViewState()
    data class Error(val exception: Exception) : ViewState()
    object Loading : ViewState()
}