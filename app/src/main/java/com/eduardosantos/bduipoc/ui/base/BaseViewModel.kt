package com.eduardosantos.bduipoc.ui.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.eduardosantos.bduipoc.ui.model.ViewState
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow

interface BaseViewModelInputs

interface BaseViewModelOutputs {
    fun viewState(): Flow<ViewState>
}

open class BaseViewModel : ViewModel(),
    BaseViewModelInputs,
    BaseViewModelOutputs {

    open val inputs: BaseViewModelInputs
        get() = this

    open val outputs: BaseViewModelOutputs
        get() = this

    var customScope = viewModelScope

    override fun onCleared() {
        super.onCleared()
        customScope.cancel()
    }

    val _viewState: MutableStateFlow<ViewState> = MutableStateFlow(ViewState.Loading)
    val viewState: SharedFlow<ViewState> = _viewState

    override fun viewState(): Flow<ViewState> {
        return viewState
    }
}