package com.eduardosantos.bduipoc.di

import android.content.Context
import android.content.res.Resources
import com.eduardosantos.bduipoc.R
import com.eduardosantos.data.api.service.BduiService
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
object BduiApiModule {

    private const val BDUI_API = "BDUI_API"
    private const val BDUI_BASE_URL = "BDUI_BASE_URL"


    @Provides
    @Singleton
    internal fun provideBduiApiService(@Named(BDUI_API) retrofit: Retrofit): BduiService {
        return retrofit.create(BduiService::class.java)
    }

    @Provides
    @Named(BDUI_API)
    internal fun provideRetrofit(
        httpBuilder: OkHttpClient.Builder,
        retrofitBuilder: Retrofit.Builder,
        @Named(BDUI_BASE_URL) baseUrl: String,
        converter: retrofit2.Converter.Factory,
        context: Context
    ): Retrofit {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpBuilder.addInterceptor(interceptor)
        httpBuilder.addInterceptor(ChuckInterceptor(context))

        return retrofitBuilder
            .client(httpBuilder.build())
            .baseUrl(baseUrl)
            .addConverterFactory(converter)
            .build()
    }

    @Provides
    @Named(BDUI_BASE_URL)
    internal fun provideBaseUrl(resources: Resources): String {
        return resources.getString(R.string.bdui_base_url)
    }
}