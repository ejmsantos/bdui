package com.eduardosantos.bduipoc.di

import com.eduardosantos.data.api.model.BduiApiResponse
import com.eduardosantos.data.api.service.BduiService
import com.eduardosantos.data.common.Mapper
import com.eduardosantos.data.repository.BduiRemoteRepository
import com.eduardosantos.data.usecase.BduiUseCase
import com.eduardosantos.domain.model.Page
import com.eduardosantos.domain.repository.BduiRepositoryType
import com.eduardosantos.domain.usecase.BduiUseCaseType
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object BduiModule {
    @Provides
    @Singleton
    fun provideBduiUseCase(
        bduiRepositoryType: BduiRepositoryType
    ): BduiUseCaseType =
        BduiUseCase(bduiRepositoryType)

    @Provides
    @Singleton
    fun provideBduiRepository(
        bduiService: BduiService,
        pageMapper: Mapper<BduiApiResponse, Page>
    ): BduiRepositoryType =
        BduiRemoteRepository(bduiService, pageMapper)
}