package com.eduardosantos.bduipoc.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {
    @Provides
    @Singleton
    fun provideContext(app: Application): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideResources(app: Application): Resources = app.resources

    @Provides
    @Singleton
    fun providePicasso(context: Context): Picasso = Picasso.Builder(context).build()
}