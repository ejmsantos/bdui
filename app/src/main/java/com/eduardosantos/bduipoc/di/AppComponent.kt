package com.eduardosantos.bduipoc.di

import android.app.Application
import com.eduardosantos.bduipoc.BduiApp
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AppModule::class,
        NetworkModule::class,
        BduiApiModule::class,
        BduiModule::class,
        MainActivityModule::class,
        MappersModule::class
    ]
)

interface AppComponent : AndroidInjector<DaggerApplication> {
    fun inject(app: BduiApp)

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(app: Application): Builder

        fun build(): AppComponent
    }
}