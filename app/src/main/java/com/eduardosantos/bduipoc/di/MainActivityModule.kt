package com.eduardosantos.bduipoc.di

import com.eduardosantos.bduipoc.ui.bdui.BduiActivity
import com.eduardosantos.bduipoc.ui.bdui.BduiFragment
import com.eduardosantos.bduipoc.ui.bdui.BduiViewModel
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainActivityModule {
    @ActivityScope
    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): BduiActivity

    @ContributesAndroidInjector
    internal abstract fun bindMainFragment(): BduiFragment

    companion object {
        @JvmStatic
        @Provides
        fun provideMainViewModelFactory(viewModel: BduiViewModel): ViewModelProviderFactory<BduiViewModel> {
            return ViewModelProviderFactory(viewModel)
        }
    }
}