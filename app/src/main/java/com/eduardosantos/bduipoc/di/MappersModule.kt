package com.eduardosantos.bduipoc.di

import com.eduardosantos.data.api.mappers.ComponentsResponseMapper
import com.eduardosantos.data.api.mappers.PageResponseMapper
import com.eduardosantos.data.api.model.BduiComponentResponse
import com.eduardosantos.data.api.model.BduiApiResponse
import com.eduardosantos.data.common.Mapper
import com.eduardosantos.domain.model.Component
import com.eduardosantos.domain.model.Page
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object MappersModule {
    @Provides
    @Singleton
    fun provideComponentsResponseMapper(): Mapper<BduiComponentResponse, Component> =
        ComponentsResponseMapper()

    @Provides
    @Singleton
    fun providePageResponseMapper(
        componentsMapper: Mapper<BduiComponentResponse, Component>
    ): Mapper<BduiApiResponse, Page> =
        PageResponseMapper(componentsMapper)
}