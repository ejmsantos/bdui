package com.eduardosantos.bduipoc

import com.eduardosantos.data.api.model.BduiApiResponse
import com.eduardosantos.domain.model.Page
import java.lang.RuntimeException

object TestConstants {
    const val TEST_EXCEPTION_MESSAGE = "Message"
    val TEST_EXCEPTION = RuntimeException(TEST_EXCEPTION_MESSAGE)
    val TEST_EMPTY_PAGE = Page(title = "Test empty page", header = null, items = emptyList())
    const val TEST_PATH = "test_path"
    const val TEST_DEEPLINK = "bdui://page?path=components"
}