package com.eduardosantos.bduipoc

import android.content.res.Resources
import app.cash.turbine.test
import com.eduardosantos.bduipoc.TestConstants.TEST_DEEPLINK
import com.eduardosantos.bduipoc.TestConstants.TEST_EMPTY_PAGE
import com.eduardosantos.bduipoc.TestConstants.TEST_EXCEPTION
import com.eduardosantos.bduipoc.TestConstants.TEST_PATH
import com.eduardosantos.bduipoc.ui.bdui.BduiViewModel
import com.eduardosantos.bduipoc.ui.model.ViewState
import com.eduardosantos.domain.usecase.BduiUseCaseType
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import kotlin.time.ExperimentalTime

@ExperimentalCoroutinesApi
@ExperimentalTime
class BduiViewModelTest {
    private val bduiUseCaseMock = Mockito.mock(BduiUseCaseType::class.java)
    private val resourcesMock = Mockito.mock(Resources::class.java)

    private val bduiViewModel = BduiViewModel().apply {
        this.bduiUseCase = bduiUseCaseMock
        this.resources = resourcesMock
        this.customScope = CoroutineScope(TestCoroutineDispatcher())
    }

    private val viewModelSpy = Mockito.spy(bduiViewModel)

    @Before
    fun before() {
        Mockito.clearInvocations(viewModelSpy)
    }

    @Test
    fun `given a valid page, when onLoadPage, then emit loading and data view states`() {
        runBlockingTest {
            bduiUseCaseMock.apply {
                Mockito.`when`(page(any())).thenReturn(TEST_EMPTY_PAGE)
            }

            viewModelSpy.viewState().test {
                viewModelSpy.onLoadPage(TEST_PATH)
                Assert.assertEquals(ViewState.Loading, expectItem())
                Assert.assertEquals(ViewState.Data(TEST_EMPTY_PAGE), expectItem())
            }

            verify(bduiUseCaseMock, Mockito.times(1)).page(TEST_PATH)
        }
    }

    @Test
    fun `given an invalid page, when onLoadPage, then emit loading and error view states`() {
        runBlockingTest {
            bduiUseCaseMock.apply {
                Mockito.`when`(page(any())).thenThrow(TEST_EXCEPTION)
            }

            viewModelSpy.viewState().test {
                viewModelSpy.onLoadPage(TEST_PATH)
                Assert.assertEquals(ViewState.Loading, expectItem())
                Assert.assertEquals(ViewState.Error(TEST_EXCEPTION), expectItem())
            }

            verify(bduiUseCaseMock, Mockito.times(1)).page(TEST_PATH)
        }
    }

    @Test
    fun `given a valid deeplink, when on onNavigateTo, then emit navigate to the deeplink`() {
        runBlockingTest {
            viewModelSpy.navigateTo().test {
                viewModelSpy.onNavigateTo(TEST_DEEPLINK)
                Assert.assertEquals(TEST_DEEPLINK, expectItem())
            }
        }
    }
}