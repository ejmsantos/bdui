package com.eduardosantos.domain.model

sealed class Component {
    data class CardComponent(
        val action: String?,
        val text: String,
        val imageUrl: String?,
    ) : Component()

    data class TextComponent(
        val text: String
    ) : Component()

    data class ButtonComponent(
        val action: String?,
        val text: String
    ) : Component()
}