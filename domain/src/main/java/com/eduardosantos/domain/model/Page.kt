package com.eduardosantos.domain.model

data class Page(val title: String, val header: String?, val items: List<Component>)