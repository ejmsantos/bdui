package com.eduardosantos.domain.repository

import com.eduardosantos.domain.model.Page

interface BduiRepositoryType {
    suspend fun getPage(pagePath: String) : Page
}