package com.eduardosantos.domain.usecase

import com.eduardosantos.domain.model.Page

interface BduiUseCaseType  {
    suspend fun page(pagePath: String) : Page
}