# Backend Driven UI (BDUI) PoC
Following my conversation with Paola about backend driven UIs I decided to do a small proof of concept using the Android Navigation Component with a single activity/fragment. 

I concentrated my efforts on the architecture(MVVM) and tests side and decided to provide a simpler UI that can be easily improved/extended.

### Frameworks:
- Dagger
- Coroutines 
- jUnit + Mockito + turbine
- Navigation Component
- Android Architecture Components
 
### UI Components subsystem
The UI components subsystem has just 3 components: `card`, `text` and `button`.

### API
The API was mocked using static pages hosted at [mockapi.io](https://mockapi.io/).

### Challenges
My initial idea was to use deep links given by the API to navigate to the next instance of the fragment. Unfortunately Android Navigation Component doesn't seem really ready to handle back stack navigation in this scenario where we keep navigating to instances of the same fragment over and over again, so for the time being the navigation is done using the action defined in the `navgraph` instead of calling `findNavController().handleDeepLink(intent)` to avoid losing the back stack.
