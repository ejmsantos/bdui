package com.eduardosantos.data.repository

import com.eduardosantos.data.api.model.BduiApiResponse
import com.eduardosantos.data.api.service.BduiService
import com.eduardosantos.data.common.Mapper
import com.eduardosantos.domain.model.Component
import com.eduardosantos.domain.model.Page
import com.eduardosantos.domain.repository.BduiRepositoryType
import java.lang.Exception

class BduiRemoteRepository(
    private val bduiApiService: BduiService,
    private val pageResponseMapper: Mapper<BduiApiResponse, Page>
) : BduiRepositoryType {

    override suspend fun getPage(pagePath: String): Page {
        val response = bduiApiService.getPage(pagePath)

        response.firstOrNull()?.let { page ->
            val newItems = mutableListOf<Component>()
            return pageResponseMapper.map(page)
        }

        throw Exception("Impossible to parse page data")
    }
}