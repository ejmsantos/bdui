package com.eduardosantos.data.api.model

import com.google.gson.annotations.SerializedName

data class BduiApiResponse(
    @SerializedName("header_image")
    val headerImage: String?,
    @SerializedName("items")
    val items: List<BduiComponentResponse>,
    @SerializedName("title")
    val title: String
)