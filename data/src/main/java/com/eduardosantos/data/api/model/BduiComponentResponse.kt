package com.eduardosantos.data.api.model

import com.google.gson.annotations.SerializedName

data class BduiComponentResponse(
    @SerializedName("action")
    val action: String?,
    @SerializedName("text")
    val text: String,
    @SerializedName("type")
    val type: String,
    @SerializedName("image")
    val image: String?
)