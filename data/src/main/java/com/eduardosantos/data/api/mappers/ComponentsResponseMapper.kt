package com.eduardosantos.data.api.mappers

import com.eduardosantos.data.api.model.BduiComponentResponse
import com.eduardosantos.data.common.Mapper
import com.eduardosantos.domain.model.Component
import java.lang.RuntimeException

class ComponentsResponseMapper : Mapper<BduiComponentResponse, Component>() {
    override fun map(from: BduiComponentResponse): Component {
        return when (from.type) {
            BUTTON_COMPONENT_TYPE -> {
                Component.ButtonComponent(
                    action = from.action,
                    text = from.text
                )
            }
            TEXT_COMPONENT_TYPE -> {
                Component.TextComponent(text = from.text)
            }
            CARD_COMPONENT_TYPE -> {
                Component.CardComponent(
                    action = from.action,
                    text = from.text,
                    imageUrl = from.image
                )
            }
            else -> throw RuntimeException(UNKNOWN_COMPONENT_EXCEPTION_MESSAGE)
        }
    }

    companion object {
        const val BUTTON_COMPONENT_TYPE = "button"
        const val TEXT_COMPONENT_TYPE = "text"
        const val CARD_COMPONENT_TYPE = "card"
        const val UNKNOWN_COMPONENT_EXCEPTION_MESSAGE = "Impossible to parse BDUI component"
    }
}