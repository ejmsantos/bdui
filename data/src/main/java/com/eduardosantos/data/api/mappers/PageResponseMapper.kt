package com.eduardosantos.data.api.mappers

import com.eduardosantos.data.api.model.BduiComponentResponse
import com.eduardosantos.data.api.model.BduiApiResponse
import com.eduardosantos.data.common.Mapper
import com.eduardosantos.domain.model.Component
import com.eduardosantos.domain.model.Page

class PageResponseMapper(private val componentsMapper: Mapper<BduiComponentResponse, Component>) :
    Mapper<BduiApiResponse, Page>() {
    override fun map(from: BduiApiResponse): Page {
        return Page(from.title, from.headerImage, componentsMapper.map(from.items))
    }
}