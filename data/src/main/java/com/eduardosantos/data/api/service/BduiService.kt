package com.eduardosantos.data.api.service

import com.eduardosantos.data.api.model.BduiApiResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface BduiService {
    @GET("/{pageId}")
    suspend fun getPage(@Path("pageId") pageId: String): List<BduiApiResponse>
}