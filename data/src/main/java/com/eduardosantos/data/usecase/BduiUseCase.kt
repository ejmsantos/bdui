package com.eduardosantos.data.usecase

import com.eduardosantos.domain.model.Page
import com.eduardosantos.domain.repository.BduiRepositoryType
import com.eduardosantos.domain.usecase.BduiUseCaseType

class BduiUseCase(private val bduiRepository: BduiRepositoryType) : BduiUseCaseType {
    override suspend fun page(pagePath: String): Page {
        return bduiRepository.getPage(pagePath)
    }
}