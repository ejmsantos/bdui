package com.eduardosantos.bduipoc

import com.eduardosantos.bduipoc.TestConstants.TEST_EMPTY_PAGE
import com.eduardosantos.bduipoc.TestConstants.TEST_EXCEPTION
import com.eduardosantos.bduipoc.TestConstants.TEST_PATH
import com.eduardosantos.data.usecase.BduiUseCase
import com.eduardosantos.domain.repository.BduiRepositoryType
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import org.mockito.Mockito

class BduiUseCaseTest {

    private val componentsRepository = Mockito.mock(BduiRepositoryType::class.java)

    private val componentsUseCase = BduiUseCase(componentsRepository)

    @ExperimentalCoroutinesApi
    @Test
    fun `given a valid page, when use case page, then return a valid page`() {
        runBlockingTest {
            componentsRepository.apply {
                Mockito.`when`(getPage(any())).thenReturn(TEST_EMPTY_PAGE)
            }

            val result = componentsUseCase.page(TEST_PATH)

            assertEquals(TEST_EMPTY_PAGE, result)

            verify(componentsRepository, Mockito.times(1)).getPage(any())
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `given an invalid page, when use case page, then thrown an exception`() {
        kotlin.runCatching {
            runBlockingTest {
                componentsRepository.apply {
                    Mockito.`when`(getPage(any())).thenThrow(TEST_EXCEPTION)
                }
                componentsUseCase.page(TEST_PATH)
                fail()
            }

        }.onFailure {
            assertEquals(TEST_EXCEPTION, it)
        }
    }
}