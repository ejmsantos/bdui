package com.eduardosantos.bduipoc

import com.eduardosantos.bduipoc.TestConstants.TEST_EMPTY_PAGE
import com.eduardosantos.bduipoc.TestConstants.TEST_EMPTY_PAGE_API_RESPONSE
import com.eduardosantos.bduipoc.TestConstants.TEST_EXCEPTION
import com.eduardosantos.bduipoc.TestConstants.TEST_PATH
import com.eduardosantos.data.api.mappers.PageResponseMapper
import com.eduardosantos.data.api.model.BduiApiResponse
import com.eduardosantos.data.api.service.BduiService
import com.eduardosantos.data.repository.BduiRemoteRepository
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Assert.assertEquals
import org.junit.Assert.fail
import org.junit.Test
import org.mockito.Mockito

class BduiRemoteRepositoryTest {
    private val apiServiceMock = Mockito.mock(BduiService::class.java)
    private val pageResponseMapperMock = Mockito.mock(PageResponseMapper::class.java).apply {
        Mockito.`when`(map(TEST_EMPTY_PAGE_API_RESPONSE.first())).thenReturn(TEST_EMPTY_PAGE)
    }
    private val surveyRepository = BduiRemoteRepository(apiServiceMock, pageResponseMapperMock)

    @ExperimentalCoroutinesApi
    @Test
    fun `given a valid page response, when use case page, then return a valid page`() {
        runBlockingTest {
            apiServiceMock.apply {
                Mockito.`when`(getPage(any())).thenReturn(TEST_EMPTY_PAGE_API_RESPONSE)
            }

            val result = surveyRepository.getPage(TEST_PATH)
            assertEquals(TEST_EMPTY_PAGE, result)

            verify(apiServiceMock, Mockito.times(1)).getPage(any())
            verify(pageResponseMapperMock, Mockito.times(1)).map(any<BduiApiResponse>())
        }
    }

    @ExperimentalCoroutinesApi
    @Test
    fun `given an invalid page response, when use case page, then an exception is thrown`() {
        kotlin.runCatching {
            runBlockingTest {
                apiServiceMock.apply {
                    Mockito.`when`(getPage(any())).thenThrow(TEST_EXCEPTION)
                }
                surveyRepository.getPage(TEST_PATH)
                fail()
            }

        }.onFailure {
            assertEquals(TEST_EXCEPTION, it)
        }
    }
}