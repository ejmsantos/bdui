package com.eduardosantos.bduipoc

import com.eduardosantos.data.api.mappers.ComponentsResponseMapper
import com.eduardosantos.data.api.mappers.ComponentsResponseMapper.Companion.BUTTON_COMPONENT_TYPE
import com.eduardosantos.data.api.mappers.ComponentsResponseMapper.Companion.CARD_COMPONENT_TYPE
import com.eduardosantos.data.api.mappers.ComponentsResponseMapper.Companion.TEXT_COMPONENT_TYPE
import com.eduardosantos.data.api.mappers.ComponentsResponseMapper.Companion.UNKNOWN_COMPONENT_EXCEPTION_MESSAGE
import com.eduardosantos.data.api.model.BduiComponentResponse
import com.eduardosantos.domain.model.Component
import org.junit.Assert.*
import org.junit.Test

class ComponentsResponseMapperTest {
    val mapper = ComponentsResponseMapper()

    @Test
    fun `given a card, when map, then get a map component`() {
        assertEquals(
            Component.CardComponent(TEST_ACTION, TEST_TEXT, TEST_IMAGE),
            mapper.map(BduiComponentResponse(TEST_ACTION, TEST_TEXT, CARD_COMPONENT_TYPE, TEST_IMAGE))
        )
    }

    @Test
    fun `given a text, when map, then get a text component`() {
        assertEquals(
            Component.TextComponent(TEST_TEXT),
            mapper.map(BduiComponentResponse(null, TEST_TEXT, TEXT_COMPONENT_TYPE, null))
        )
    }

    @Test
    fun `given a button, when map, then get a button component`() {
        assertEquals(
            Component.ButtonComponent(TEST_ACTION, TEST_TEXT),
            mapper.map(BduiComponentResponse(TEST_ACTION, TEST_TEXT, BUTTON_COMPONENT_TYPE, null))
        )
    }

    @Test
    fun `given an invalid component, when map, then throw exception`() {
        kotlin.runCatching {
            mapper.map(BduiComponentResponse(TEST_ACTION, TEST_TEXT, "unknown", null))
        }.onFailure {
            assertEquals(UNKNOWN_COMPONENT_EXCEPTION_MESSAGE, it.message)
        }
    }

    companion object {
        const val TEST_ACTION = "TEST_ACTION"
        const val TEST_TEXT = "TEST_TEXT"
        const val TEST_IMAGE = "TEST_IMAGE"
    }
}