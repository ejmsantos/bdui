package com.eduardosantos.bduipoc

import com.eduardosantos.bduipoc.TestConstants.TEST_EMPTY_PAGE
import com.eduardosantos.bduipoc.TestConstants.TEST_EMPTY_PAGE_API_RESPONSE
import com.eduardosantos.data.api.mappers.ComponentsResponseMapper
import com.eduardosantos.data.api.mappers.PageResponseMapper
import com.eduardosantos.data.api.model.BduiComponentResponse
import com.nhaarman.mockito_kotlin.any
import org.junit.Test
import org.junit.Assert.*
import org.mockito.Mockito

class PageResponseMapperTest {

    private val componentsMapperMock = Mockito.mock(ComponentsResponseMapper::class.java).apply {
        Mockito.`when`(map(any<List<BduiComponentResponse>>())).thenReturn(emptyList())
    }

    private val mapper = PageResponseMapper(componentsMapperMock)

    @Test
    fun `given a page, when map, then get a page component`() {
        assertEquals(TEST_EMPTY_PAGE, mapper.map(TEST_EMPTY_PAGE_API_RESPONSE.first()))
    }
}